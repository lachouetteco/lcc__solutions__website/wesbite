export default () => {
  window.axeptioSettings = {
    clientId: "6227665c0bf064210f9485f3",
    cookiesVersion: "la chouette co-fr",
  };

  (function (d, s) {
    var t = d.getElementsByTagName(s)[0];
    var e = d.createElement(s);
    e.async = true;
    e.src = "//static.axept.io/sdk.js";
    t.parentNode.insertBefore(e, t);
  })(document, "script");

  window.$crisp = [];
  window.CRISP_WEBSITE_ID = "26704ddb-8a8d-4117-a4d0-4daed8799e55";

  (function () {
    var d = document;
    var s = d.createElement("script");
    s.src = "https://client.crisp.chat/l.js";
    s.async = 1; d.getElementsByTagName("head")[0].appendChild(s);
  })();

  (function (w, d, s, l, i) {
    w[l] = w[l] || []; w[l].push({
      'gtm.start':
  new Date().getTime(),
      event:'gtm.js',
    }); var f = d.getElementsByTagName(s)[0];
    var j = d.createElement(s); var dl = l !== 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
  'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
  })(window, document, 'script', 'dataLayer', 'GTM-MF3BTVG');

  /* eslint-disable */
    function loadGoogleAnalyticsTag () {
      var d = document;
      var j = d.createElement("script");
      j.src = 'https://www.googletagmanager.com/gtag/js?id=G-H0Z44E2Z3E';
      j.id = "analyticsScript";
      d.getElementsByTagName("head")[0].appendChild(j);


      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-H0Z44E2Z3E');
    }

    setTimeout(function () {
      window._axcb?.push(function (axeptio) {
        axeptio.on("cookies:complete", function (choices) {
          if (process.env.NODE_ENV === 'production') {
            if (choices.google_analytics) {
              loadGoogleAnalyticsTag();
            } else {
              setTimeout(function () {
                const script = document.getElementById("analyticsScript");
                if (script) {
                  script.parentNode.removeChild(script);
                }
              }, 10);
            }

            // if (choices.crisp) {
            //   loadCrisp();
            // } else {
            //   setTimeout(function () {
            //     const script = document.getElementById("crispScript");
            //     if (script) {
            //       script.parentNode.removeChild(script);
            //     }
            //   }, 1000);
            // }
          }
        });
      });
    }, 1000);
}
