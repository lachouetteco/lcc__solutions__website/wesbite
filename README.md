<div align="center">
        <a href="https://www.lachouette.co">
            <img alt="Logo" src="./.readme/logo.svg" width="240" >
        </a>
        <hr>
</div>
<div align="center">
        <h1>Website</h1>
        <p>🦉 🌍</p>
</div>
<br>

## Description
La Chouette Co is a Digital product studio that helps companies build human, useful and sustainable digital products. Our website is the virtual gate to our company from everywhere. Nerver finished, always tweaked to tailor the best experience for our people and yours. 

This website is based on [NuxtJS framework](https://nuxtjs.org). For detailed explanation on how things work, check out the [nuxt documentation](https://nuxtjs.org).

## Build setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

## Company utils & Assets
We use our website to share some tools & assets with our community (inside our outside the company)

### `Assets`
Our community can access to high quality assets directly on our website.

Example : `/static/assets/logos/https://www.lachouette.co/assets/logos/logo__type__primary__lachouetteco--animated.gif`

### `Email signature`
Every employee and partner can find an example of our email signature that meet our brand guidelines. Always up to date, it can be copied and pasted into your favorite email app.

Folder : `/static/company-utils/email-signature.html`

## Thanks
Thanks go out to our marketing, engineering and design team.

A big part of what makes La Chouette Co great is each and every one our employees and partners. Your contributions enrich the Chouette experience and make it better every day.



<br>
<div align="center">
        <a href="https://www.lachouette.co" target="_blank">
            <strong style="color:black">Design & Tech by &nbsp; &nbsp;</strong><img alt="Made with love by La Chouette Co" src="https://www.lachouette.co/assets/logos/logo__type__primary__lachouetteco--animated.gif" height="20" >
        </a>
</div>

