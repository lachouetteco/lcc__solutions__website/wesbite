/*
** TailwindCSS Configuration File
**
** Docs: https://tailwindcss.com/docs/configuration
** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
*/
module.exports = {
  theme: {
    fontFamily: {
      'sans': ['ui-sans-serif', 'system-ui', '-apple-system', 'BlinkMacSystemFont', 'Segoe UI', 'Roboto', 'Helvetica Neue', 'Arial', 'Noto Sans', 'sans-serif', 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji'],
    },
    screens: {
      'print': { 'raw': 'print' },
      xs: '321px',
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
      '2xl': '1536px',
    },
    colors: {
      white: '#fff',
      black: '#000',
      gray: {
        900: '#101010',
        800: '#272727',
        700: '#3E3E3E',
        600: '#545454',
        default: '#818181',
        400: '#989898',
        300: '#AFAFAF',
        200: '#C5C5C5',
        100: '#DCDCDC',
        50: '#F3F3F3',
      },
      primary: {
        900: '#22177D',
        800: '#291C99',
        700: '#3423BF',
        600: '#3E29E6',
        default: '#452EFF',
        400: '#7D6DFF',
        300: '#B5ABFF',
        200: '#D1CBFF',
        100: '#ECEAFF',
        50: '#F6F5FF',
      },
      secondary: {
        900: '#0B706A',
        800: '#0E8982',
        700: '#11ACA2',
        600: '#15CEC2',
        501: '#18F6E8',
        default: '#5DEDE4',
        400: '#5DEDE4',
        300: '#A2F5EF',
        200: '#C5F9F5',
        100: '#E8FCFB',
        50: '#F3FEFD',
      },
    },
    extend: {
      backgroundColor: {
        transparent: 'transparent',
      },
      borderColor: {
        transparent: 'transparent',
      },
    },
  },
  variants: {
    display: ['responsive', 'hover', 'focus', 'group-hover'],
    fontSize: ['responsive', 'hover', 'focus', 'group-hover'],
    lineHeight: ['responsive', 'hover', 'focus', 'group-hover'],
    height: ['responsive', 'hover', 'focus', 'group-hover'],
    width: ['responsive', 'hover', 'focus', 'group-hover'],
    textColor: ['responsive', 'hover', 'focus', 'group-hover'],
    boxShadow: ['responsive', 'hover', 'focus', 'group-focus'],
    borderColor: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
    borderWidth: ['responsive', 'hover', 'focus', 'group-hover'],
    borderStyle: ['responsive', 'hover', 'focus', 'group-hover'],
    scale: ['hover', 'group-hover'],
    backgroundOpacity: ['responsive', 'hover', 'focus', 'group-hover'],
    inset: ['responsive', 'hover', 'focus', 'group-hover'],
    translate: ['responsive', 'hover', 'focus', 'group-hover'],
  },
  plugins: [],
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: true,
    content: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      'nuxt.config.js',
    ],
  },
};
